package com.orengy.merrylights.common

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@ExperimentalUnsignedTypes
class ColorTests {
    @Test
    fun testRGB() {
        assertEquals(Color.fromRGB(255, 255, 255).hex, "#FFFFFF")
        assertEquals(Color.fromRGB(0, 0, 0).hex, "#000000")
        assertEquals(Color.fromRGB(255, 0, 0).hex, "#FF0000")
        assertEquals(Color.fromRGB(0, 255, 0).hex, "#00FF00")
        assertEquals(Color.fromRGB(0, 0, 255).hex, "#0000FF")
        assertEquals(Color.fromRGB(255, 255, 255).hexWithAlpha, "#FFFFFFFF")
        assertEquals(Color.fromRGB(255, 255, 255, 0).hexWithAlpha, "#00FFFFFF")

        assertEquals(Color.fromHSV(330, 100, 100).hex, "#FF0080")
        assertEquals(Color.fromHSV(300, 100, 100).hex, "#FF00FF")
        assertEquals(Color.fromHSV(50, 50, 50).hex, "#807540")
        assertEquals(Color.fromHSV(50, 50, 0).hex, "#000000")

        assertEquals(Color.fromHSL(330, 100, 100).hex, "#FFFFFF")
        assertEquals(Color.fromHSL(300, 100, 100).hex, "#FFFFFF")
        assertEquals(Color.fromHSL(50, 50, 50).hex, "#BFAA40")
        assertEquals(Color.fromHSL(50, 50, 0).hex, "#000000")

        assertEquals(Color.fromHex("#000000").hex, "#000000")
        assertEquals(Color.fromHex("#000000").hexWithAlpha, "#FF000000")
        assertEquals(Color.fromHex("#00000000").hexWithAlpha, "#00000000")
        assertEquals(Color.fromHex("#123456").hex, "#123456")
        assertEquals(Color.fromHex("#789aBcDe").hexWithAlpha, "#789ABCDE")
    }
}