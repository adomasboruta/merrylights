package com.orengy.merrylights.common

import kotlin.math.abs
import kotlin.math.round

class Color private constructor(
        val color: Int
) {
    companion object {
        fun fromHex(hex: String): Color {
            val hexRegex = Regex("^#?([0-9a-fA-F]{6}|[0-9a-fA-F]{8})$")
            check(hexRegex.matches(hex))
            val str = hexRegex.matchEntire(hex)!!.groups[1]!!.value
            return if (str.length == 6) {
                Color(0xFF shl 24 or str.toInt(16))
            } else {
                Color(str.substring(0, 2).toInt(16) shl 24 or str.substring(2).toInt(16))
            }
        }

        fun fromRGB(r: Int, g: Int, b: Int, a: Int = 255): Color {
            check(r in 0..255)
            check(g in 0..255)
            check(b in 0..255)
            check(a in 0..255)

            return Color((((((a shl 8) or r) shl 8) or g) shl 8) or b)
        }

        fun fromHSL(h: Int, s: Int, l: Int, a: Int = 255): Color {
            check(h in 0..359)
            check(s in 0..100)
            check(l in 0..100)
            check(a in 0..255)

            val H = h / 60.0
            val S = s / 100.0
            val L = l / 100.0

            val C = (1 - abs(2 * L - 1)) * S

            val X = C * (1 - abs(H % 2 - 1))

            val (R1, G1, B1) = when (H) {
                in 0.0..1.0 -> Triple(C, X, 0.0)
                in 1.0..2.0 -> Triple(X, C, 0.0)
                in 2.0..3.0 -> Triple(0.0, C, X)
                in 3.0..4.0 -> Triple(0.0, X, C)
                in 4.0..5.0 -> Triple(X, 0.0, C)
                in 5.0..6.0 -> Triple(C, 0.0, X)
                else -> Triple(0.0, 0.0, 0.0)
            }


            val m = L - C / 2;
            val (R, G, B) = Triple(
                    round((R1 + m) * 255.0).toInt(),
                    round((G1 + m) * 255.0).toInt(),
                    round((B1 + m) * 255.0).toInt()
            )
            return fromRGB(R, G, B, a)
        }

        fun fromHSV(h: Int, s: Int, v: Int, a: Int = 255): Color {
            check(h in 0..359)
            check(s in 0..100)
            check(v in 0..100)
            check(a in 0..255)

            val H = h / 60.0
            val S = s / 100.0
            val V = v / 100.0

            val C = V * S
            val X = C * (1 - abs(H % 2 - 1))

            val (R1, G1, B1) = when (H) {
                in 0.0..1.0 -> Triple(C, X, 0.0)
                in 1.0..2.0 -> Triple(X, C, 0.0)
                in 2.0..3.0 -> Triple(0.0, C, X)
                in 3.0..4.0 -> Triple(0.0, X, C)
                in 4.0..5.0 -> Triple(X, 0.0, C)
                in 5.0..6.0 -> Triple(C, 0.0, X)
                else -> Triple(0.0, 0.0, 0.0)
            }

            val m = V - C
            val (R, G, B) = Triple(
                    round((R1 + m) * 255.0).toInt(),
                    round((G1 + m) * 255.0).toInt(),
                    round((B1 + m) * 255.0).toInt()
            )

            return fromRGB(R, G, B, a)
        }
    }

    val hex by lazy {
        "#${(color and 0x00FFFFFF).toString(16).padStart(6, '0')}".toUpperCase()
    }

    @ExperimentalUnsignedTypes
    val hexWithAlpha by lazy {
        "#${color.toUInt().toString(16).padStart(8, '0')}".toUpperCase()
    }

}