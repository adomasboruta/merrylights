package com.orengy.merrylights.common

import kotlin.math.abs
import kotlin.math.round

class LightShow(
        val lightCount: Int
) {

    enum class Type {
        RAINBOW, COLOR_WHEEL, FADE
    }

    fun render(
            type: Type,
            timer: Long,
            speed: Int,
            params: Map<String, Any> = mapOf(),
            handler: (Int, Color) -> Unit) {
        val counter = (timer / speed).toInt()
        when (type) {
            Type.RAINBOW -> {
                val step = if (params.containsKey("step") && params["step"] is Int)
                    params["step"] as Int
                else
                    20
                renderRainbow(counter, step, handler)
            }
            Type.COLOR_WHEEL -> {
                val colors = params["colors"]
                if (colors != null) {
                    if (colors is List<*>)
                        for (item in colors) {
                            check(item is Int && item in 0..359)
                        }
                    renderCustomColorWheel(
                            counter,
                            (timer % speed) / speed.toDouble(),
                            colors as List<Int>,
                            handler
                    )
                } else {
                    renderColorWheel(counter, handler)
                }

            }
            Type.FADE -> renderFade(counter, handler)
        }
    }

    private fun renderRainbow(counter: Int, colorStep: Int, handler: (Int, Color) -> Unit) {
        val colours = Array(colorStep) {
            Color.fromHSV(360 * it / colorStep, 100, 100)
        }
        for (pixel in 0 until lightCount) {
            handler(pixel, colours[((counter + pixel) % colours.size)])
        }
    }

    private fun renderColorWheel(counter: Int, handler: (Int, Color) -> Unit) {
        val color = Color.fromHSV((counter % 360), 100, 100)
        for (pixel in 0 until lightCount) {
            handler(pixel, color)
        }
    }

    private fun renderCustomColorWheel(counter: Int, progress: Double, hueList: List<Int>, handler: (Int, Color) -> Unit) {
        val start = hueList[counter % hueList.size]
        val end = hueList[(counter + 1) % hueList.size] + if (hueList[(counter + 1) % hueList.size] < start) 360 else 0
        val color = Color.fromHSV(
                linearInterpolation(start, end, progress) % 360, 100, 100)
        for (pixel in 0 until lightCount) {
            handler(pixel, color)
        }
    }

    private fun renderFade(counter: Int, handler: (Int, Color) -> Unit) {
        val color = Color.fromHSV(((counter / 201)*30 % 360), 100, abs(abs(counter % 201 - 100) - 100))
        for (pixel in 0 until lightCount) {
            handler(pixel, color)
        }
    }

    private fun linearInterpolation(start: Int, end: Int, progress: Double): Int {
        return round(start + (end - start) * progress).toInt()
    }
}