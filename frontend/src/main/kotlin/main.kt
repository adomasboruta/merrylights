import merrylights.app
import merrylights.indexPage
import react.dom.render
import tictactoe2.game
import kotlin.browser.document
import kotlin.browser.window

fun main(args: Array<String>) {
    window.onload = {
        val root = document.getElementById("root") ?: throw IllegalStateException()
        render(root) {
            app()
        }
    }
}
