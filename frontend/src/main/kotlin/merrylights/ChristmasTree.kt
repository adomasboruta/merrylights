package merrylights

import com.orengy.merrylights.common.LightShow
import kotlinx.css.*
import react.*
import react.dom.jsStyle
import styled.StyleSheet
import styled.css
import styled.styledDiv
import styled.styledImg
import kotlin.browser.window
import kotlin.js.Date

class ChristmasTree : RComponent<ChristmasTree.Props, ChristmasTree.State>() {

    override fun componentDidMount() {
        setState {
            animationIntervalId = window.setInterval({ animateLights() }, props.animationSpeed)
            animationTime = Date.now().toLong()
        }

    }

    override fun componentWillUnmount() {
        window.clearInterval(state.animationIntervalId)
    }

    override fun RBuilder.render() {
        styledDiv {
            styledDiv {
                css {
                    position = Position.relative
                }

                styledImg(src = "img/christmas-tree-pink2.png") {
                    css {
                        width = 100.pct
                    }
                }

                for ((_, pixel) in state.pixelColors) {
                    styledDiv {
                        css {
                            +pixel.style
                        }
                        attrs.jsStyle {
                            backgroundColor = pixel.color.value
                        }
                    }
                }
            }
        }
    }


    interface Props : RProps {
        var lightShow: LightShow.Type
        var animationSpeed: Int
    }

    interface State : RState {

        var animationTime: Long
        var animationIntervalId: Int
        var pixelColors: Map<Int, LightPixel>
    }

    val lightCount = 114
    val imageSize = Pair(607, 600)
    val lightWidth = (14 * window.innerWidth / imageSize.first) + ((14 * window.innerWidth / imageSize.first) % 2)

    inner class LightPixel(
            val x: Int,
            val y: Int,
            var color: Color = Color.black
    ) : StyleSheet("point-$x-$y", isStatic = true) {

        val style by css {
            width = lightWidth.px
            height = lightWidth.px
            marginLeft = (lightWidth / -2).px
            marginTop = (lightWidth / -2).px
            position = Position.absolute
            left = (x * 100.0 / imageSize.first).pct
            top = (y * 100.0 / imageSize.second).pct
            borderRadius = 100.pct
        }

    }

    val lightShow = LightShow(lightCount)

    fun animateLights() {
        setState {
            lightShow.render(
                    type = props.lightShow,
                    speed = 100,
                    timer = Date.now().toLong() - animationTime) { index, color ->
                if (pixelColors.containsKey(index)) {
                    pixelColors[index]!!.color = Color(color.hex)
                }
            }


        }
    }

    init {
        state.apply {
            animationTime = 0
            pixelColors = mapOf(
                    0 to LightPixel(67, 484),
                    1 to LightPixel(101, 487),
                    2 to LightPixel(134, 492),
                    3 to LightPixel(170, 496),
                    4 to LightPixel(203, 497),
                    5 to LightPixel(235, 497),
                    6 to LightPixel(270, 498),
                    7 to LightPixel(306, 498),
                    8 to LightPixel(343, 493),
                    9 to LightPixel(377, 486),
                    10 to LightPixel(405, 479),
                    11 to LightPixel(441, 466),
                    12 to LightPixel(470, 454),
                    13 to LightPixel(491, 444),
                    28 to LightPixel(104, 428),
                    29 to LightPixel(135, 429),
                    30 to LightPixel(164, 431),
                    31 to LightPixel(192, 433),
                    32 to LightPixel(228, 432),
                    33 to LightPixel(260, 432),
                    34 to LightPixel(293, 430),
                    35 to LightPixel(325, 425),
                    36 to LightPixel(357, 418),
                    37 to LightPixel(389, 408),
                    38 to LightPixel(420, 394),
                    39 to LightPixel(449, 378),
                    52 to LightPixel(140, 364),
                    53 to LightPixel(161, 365),
                    54 to LightPixel(189, 364),
                    55 to LightPixel(218, 365),
                    56 to LightPixel(251, 363),
                    57 to LightPixel(281, 360),
                    58 to LightPixel(308, 356),
                    59 to LightPixel(337, 349),
                    60 to LightPixel(360, 339),
                    61 to LightPixel(392, 322),
                    62 to LightPixel(412, 308),
                    74 to LightPixel(181, 295),
                    75 to LightPixel(217, 297),
                    76 to LightPixel(245, 297),
                    77 to LightPixel(274, 296),
                    78 to LightPixel(307, 290),
                    79 to LightPixel(334, 281),
                    80 to LightPixel(358, 269),
                    81 to LightPixel(384, 254),
                    90 to LightPixel(217, 244),
                    91 to LightPixel(245, 243),
                    92 to LightPixel(277, 239),
                    93 to LightPixel(305, 233),
                    94 to LightPixel(331, 223),
                    95 to LightPixel(350, 209),
                    102 to LightPixel(256, 201),
                    103 to LightPixel(283, 198),
                    104 to LightPixel(307, 192),
                    105 to LightPixel(333, 175),
                    110 to LightPixel(279, 160),
                    111 to LightPixel(301, 152),
                    112 to LightPixel(317, 143)
            )
        }
    }

}

fun RBuilder.christmasTree(
        lightShow: String,
        animationSpeed: Int = 100
) = child(ChristmasTree::class) {
    attrs.lightShow = LightShow.Type.valueOf(lightShow)
    attrs.animationSpeed = animationSpeed
}
