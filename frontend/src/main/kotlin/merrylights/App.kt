package merrylights

import com.ccfraser.muirwik.wrapper.*
import com.ccfraser.muirwik.wrapper.form.mFormControl
import com.ccfraser.muirwik.wrapper.input.mInputLabel
import com.ccfraser.muirwik.wrapper.styles.Palette
import com.ccfraser.muirwik.wrapper.styles.PaletteOptions
import com.ccfraser.muirwik.wrapper.styles.ThemeOptions
import kotlinext.js.js
import kotlinext.js.jsObject
import kotlinx.css.CSSBuilder
import kotlinx.css.Color
import kotlinx.css.body
import kotlinx.css.px
import org.w3c.dom.events.Event
import react.*
import styled.*


class App : RComponent<RProps, RState>() {

    override fun RBuilder.render() {
        mCssBaseline()
        @Suppress("UnsafeCastFromDynamic")
        val themeProps: ThemeOptions = jsObject {
            palette = jsObject {
                type = "light"
                primary = js {
                    main = Colors.Red.shade700.toString()
                }
                secondary = js {
                    main = Colors.Red.accent200.toString()
                }
                background = js {
                    default = "#FFDDD3"
                }
            }

            typography = jsObject {
                useNextVariants = true
            }
        }
        @Suppress("UnsafeCastFromDynamic")
        currentTheme = createMuiThemeFunction(themeProps)
        mMuiThemeProvider(currentTheme)
        {
            indexPage()
        }
    }
}

fun RBuilder.app() = child(App::class) {}
