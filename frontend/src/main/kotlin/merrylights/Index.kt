package merrylights

import com.ccfraser.muirwik.wrapper.*
import com.ccfraser.muirwik.wrapper.form.mFormControl
import com.orengy.merrylights.common.LightShow
import kotlinext.js.js
import kotlinx.css.Position
import kotlinx.css.margin
import kotlinx.css.pct
import kotlinx.css.px
import react.*
import styled.css
import styled.styledDiv
import kotlin.browser.window


class IndexPage : RComponent<RProps, IndexPage.State>() {

    init {
        state.apply {
            selectedLightShow = LightShow.Type.RAINBOW.name
            rpiLightShow = LightShow.Type.RAINBOW.name
        }
    }

    override fun componentDidMount() {
        window.fetch("/lightshow")
                .then { resp -> resp.json() }
                .then { data ->
                    setState {
                        selectedLightShow = data.asDynamic().type as String
                        rpiLightShow = selectedLightShow
                    }

                    console.log(data)
                }
    }

    override fun RBuilder.render() {
        mAppBar(position = MAppBarPosition.relative) {
            mToolbar {
                mTypography(variant = MTypographyVariant.h6, color = MTypographyColor.inherit) {
                    +"Merry lights"
                }
            }
        }

        styledDiv {
            css {
                margin(1.spacingUnits)
            }

        }

        christmasTree(state.selectedLightShow, 30)

        styledDiv {
            css {
                margin(1.spacingUnits)
            }

            mTypography {
                +"Current light show RPI: ${state.rpiLightShow}"
            }

            mFormControl {
                css {
                    width = 100.pct
                }
                mSelect(state.selectedLightShow, onChange = { event, _ -> setState { selectedLightShow = event.targetValue as String } }) {
                    for (t in LightShow.Type.values()) {
                        mMenuItem(t.name, value = t.name)
                    }
                }
            }

            mButtonFab("done", color = MColor.secondary, onClick = { event ->
                val body = js{}
                body.type = state.selectedLightShow

                val req = js {
                    method = "PUT"
                    headers = js{}
                    headers["Content-Type"] = "application/json"
                }
                req.body = JSON.stringify(body)



                window.fetch("/lightshow", req)
                        .then { resp -> resp.json() }
                        .then { data ->
                            setState {
                                selectedLightShow = data.asDynamic().type as String
                                rpiLightShow = selectedLightShow
                            }

                            console.log(data)
                        }
            }) {
                css {
                    position = Position.fixed
                    right = 0.px
                    bottom = 0.px
                    margin(1.spacingUnits)
                }
            }
        }

    }

    interface State : RState {
        var selectedLightShow: String
        var rpiLightShow: String
    }
}

fun RBuilder.indexPage() = child(IndexPage::class) {}
