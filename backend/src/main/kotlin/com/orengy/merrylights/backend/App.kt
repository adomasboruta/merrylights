package com.orengy.merrylights.backend

import com.diozero.ws281xj.StripType
import com.diozero.ws281xj.rpiws281x.WS281x
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.routing.*
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.orengy.merrylights.common.LightShow
import io.ktor.jackson.*
import io.ktor.features.*
import io.ktor.http.content.defaultResource
import io.ktor.http.content.resources
import io.ktor.http.content.static
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.File


data class LightShowDTO(val type: LightShow.Type, val data: Map<String, Any> = mapOf())

val objectMapper = ObjectMapper().registerKotlinModule().enable(SerializationFeature.INDENT_OUTPUT)

val currentStateFile = File("currentState.json")
@Volatile
var currentState = if (currentStateFile.exists()) {
    objectMapper.readValue(currentStateFile, LightShowDTO::class.java)
} else {
    LightShowDTO(LightShow.Type.COLOR_WHEEL)
}

fun main(args: Array<String>): Unit = io.ktor.server.jetty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(AutoHeadResponse)
    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
        }
    }
    install(DataConversion)

    routing {
        static {
            resources("static")
            defaultResource("static/index.html")
        }

        get("/lightshow") {
            call.respond(currentState)
        }

        put("/lightshow") {
            currentState = call.receive()
            currentStateFile.writeText(objectMapper.writeValueAsString(currentState))
            call.respond(currentState)
        }
    }

    GlobalScope.launch {
        val gpioNum = 18
        val brightness = 64    // 0..255
        val lightCount = 102

        val lightShow = LightShow(lightCount)

        val animationTime = System.currentTimeMillis()

        try {
            WS281x(
                    800_000,
                    5,
                    gpioNum,
                    brightness,
                    lightCount,
                    StripType.WS2811_RGB,
                    0
            ).use { ledDriver ->
                while (true) {
                    lightShow.render(
                            type = currentState.type,
                            speed = 100,
                            timer = System.currentTimeMillis() - animationTime) { index, color ->
                        ledDriver.setPixelColour(index, color.color)
                    }
                    ledDriver.render()
                    delay(100L)
                }
            }
        } catch (t: Throwable) {
            println("Error: $t")
            t.printStackTrace()
        }
    }
}
