#Merry lights
```
scp backend/build/libs/merrylights.jar dietpi@192.168.2.108:/home/dietpi
scp merrylights.sh dietpi@192.168.2.108:/home/dietpi
scp merrylights.service root@192.168.2.108:/etc/systemd/system
sudo systemctl restart merrylights
```

##Next steps
###Effects
* color wipe easy
* fade in out and change color
* remake rainbow based on hue with some step along the wheel

###parameters:
* speed
* color picker + static color
* custom profiles based on effects with colors, need to persist. db?